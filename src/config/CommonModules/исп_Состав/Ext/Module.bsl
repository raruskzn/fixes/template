﻿// Состав правок (исправлений)
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает набор доступных правок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Массив
//
Функция Получить() Экспорт
	
	Результат	= Новый Массив();
	
	Результат.Добавить(___Пример___());
	
	Возврат Результат;
	
КонецФункции // Получить 

// Возвращает описание правки вдимости вида оплаты СБП в документе ЧекККМ
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ___Пример___() Экспорт
	
	Результат	= исп_Описание.Создать();
	
	Текст		= "Пример определения правки";
	
	исп_Описание.Идентификатор(Результат, исп_Идентификатор.___Пример___());
	исп_Описание.Вид(Результат, исп_Вид.Функционал());
	исп_Описание.Область(Результат, исп_Область.Продажи());
	исп_Описание.ВерсияМин(Результат, "9.9.9.0001");
	исп_Описание.ВерсияМакс(Результат, "9.9.9");
	исп_Описание.Текст(Результат, Текст);
	исп_Описание.Методы(Результат, "ОМ.Пример.ИзменяемыйМетод");
	
	Возврат Результат;
	
КонецФункции // ___Пример___ 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 