﻿// BSLLS:CommonModuleNameClientServer-off - осмысленно, постфикс не имеет смысла
// Идентификаторы правок
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает идентификатор правки Пример
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ___Пример___() Экспорт
	
	Возврат "___Пример___";
	
КонецФункции // ___Пример___ 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
// BSLLS:CommonModuleNameClientServer-oт
