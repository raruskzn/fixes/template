﻿// Информация о текущей конфигурации
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Истина, если правки совместимы с текущей конфигурацией
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Булево
//
Функция Совместима() Экспорт
	
	// Необходимо заменить на корректный код,
	// возвращающий признак совместимости расширения 
	// с текущей конфигурацией
	
	// BSLLS:UnreachableCode-off
	ВызватьИсключение "Необходим алгоритм определения совместимости конфигурации";
	
	Возврат Метаданные.Имя = Идентификатор() 
	И исп_Версия.Сравнить(Версия(), "2.5.10", 3) = 0;
	// BSLLS:UnreachableCode-on
	
КонецФункции // Совместима 

// Возвращает идентификатор модифицируемой конфигурации
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция Идентификатор() Экспорт
	
	// Необходимо заенить на корректный код, 
	// возвращающий идентификатор модифицируемой конфигурации. 
	// НЕ используется в качестве фильтра, служит информационной строкой
	
	// BSLLS:UnreachableCode-off
	ВызватьИсключение "Необходим алгоритм определения имени конфигурации";
	
	Возврат "УправлениеПредприятием";
	// BSLLS:UnreachableCode-on
	
КонецФункции // Идентификатор 

// Возвращает версию модифицируемой конфигурации
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция Версия() Экспорт
	
	// Необходимо заменить на корректный код,
	// возвращающий текущую версию модифицируемой конфигурации (библиотеки)
	// Используется при определении совместимости правок с текущей версией
	// модифицируемой конфигурации (библиотеки)
	
	// BSLLS:UnreachableCode-off
	ВызватьИсключение "Необходим алгоритм определения версии";
	
	Возврат Метаданные.Версия;
	// BSLLS:UnreachableCode-on
	
КонецФункции // Версия 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 